# App submission request for `APP_NAME`

<!-- Please fill the form below in every part -->

## Summary

- [ ] I am the author or a contributor of this app
- [ ] This app uses a [GTK](https://gtk.org) UI
- [ ] This app follows the [GNOME HIG](https://developer.gnome.org/hig/stable/)
- [ ] This app has been designed to work on mobile Linux devices
- [ ] This app is available as a Flatpak

- **App website (if applicable)**: (...)
- **Code repository**: (...)
- **Language(s) this app has been developed with**: (...)
- **Flathub link (if applicable)**: (...)
- **Very short app description**: (...)

## Additional info (optional)

### How have you found this website?

(...)

### Why are you interested in having your app listed?

(...)

### Do you want to write a more detailed description of your app?

(...)

### Any feedback or suggestions to improve this website?

(...)

<!-- Thank you for your submission! -->

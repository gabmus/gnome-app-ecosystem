function createAppCard(app) {
    return `
    <div class="app_card">
        <img src="${app.icon}" class="app_icon" />
        <h2>${app.name}</h2>
        <p class="description">${app.description}</p>
        <div class="features">
            <div class="feature_tooltip">
                <span class="tooltip">
                    Designed for Linux phones
                </span>
                <img
                    src="./assets/icons/mobile.svg"
                    style="display:
                        ${app.features.mobile_friendly ?
                        'inline-block' : 'none'}
                    "
                />
            </div>
            <div class="feature_tooltip">
                <span class="tooltip">
                    Available on Flathub
                </span>
                <a href="${app.flathub || '#app_gallery'}"
                    style="display:
                        ${app.features.flatpak ?
                        'inline-block' : 'none'}
                    ">
                    <img
                        src="./assets/icons/flatpak.svg"
                    />
                </a>
            </div>
        </div>
        <div class="card_buttons">
            <a target="_blank" href="${app.url}">Website</a>
            <a target="_blank" href="${app.source}">Source</a>
        </div>
    </div>`;
}

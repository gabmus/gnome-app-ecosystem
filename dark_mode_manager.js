function getCookie(name) {
    for (let cookie of document.cookie.split(';')) {
        cookie = cookie.trim();
        if (cookie.split('=')[0] == name) {
            return cookie.split('=')[1].replace(';', '').trim();
        }
    }
    console.log('no cookie');
    return null;
}

function setCookie(name, value) {
    document.cookie = `${name}=${value}`;
}

function darktoggle() {
    let body = document.getElementsByTagName('body')[0];
    body.className = body.className == 'darkmode' ? '' : 'darkmode';
    setCookie(
        'org.gabmus.darkmode',
        body.className == 'darkmode' ? '1' : '0'
    );
}

var browserPrefersDarkMode = window.getComputedStyle(
    document.getElementById('color_preference_detector')
).content === '"dark"';

if (getCookie('org.gabmus.darkmode') == null) {
    setCookie(
        'org.gabmus.darkmode',
        browserPrefersDarkMode ? '1' : '0'
    );
}

if (getCookie('org.gabmus.darkmode') == '1') {
    darktoggle();
}
